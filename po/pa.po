# Punjabi translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-20 09:02+0000\n"
"PO-Revision-Date: 2015-06-17 14:19+0000\n"
"Last-Translator: Gursharnjit_Singh <ubuntuser13@gmail.com>\n"
"Language-Team: Punjabi <pa@li.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"
"X-Generator: Launchpad (build 17908)\n"

#: ../src/app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "ਚੁਣੋ"

#: ../src/app/qml/AlternateActionPopover.qml:73
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "ਕਾਪੀ"

#: ../src/app/qml/AlternateActionPopover.qml:79
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "ਚੇਪੋ"

#: ../src/app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:99
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:151
#: ../src/app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "ਨਵੀਂ ਟੈਬ"

#: ../src/app/qml/AlternateActionPopover.qml:104
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr ""

#: ../src/app/qml/AlternateActionPopover.qml:109
msgid "Close App"
msgstr ""

#: ../src/app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਦੀ ਲੋੜ"

#: ../src/app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr ""

#: ../src/app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr ""

#: ../src/app/qml/AuthenticationDialog.qml:58
#, fuzzy
msgid "Authenticate"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/AuthenticationDialog.qml:70
#: ../src/app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "ਰੱਦ"

#: ../src/app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "ਪ੍ਰਮਾਣਿਕਤਾ ਫ਼ੇਲ੍ਹ ਹੋਈ"

#: ../src/app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr ""

#: ../src/app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""

#: ../src/app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr ""

#: ../src/app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "ਕੀਬੋਰਡ ਬਦਲੋ"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr ""

#. TRANSLATORS: This is the name of the Control key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr ""

#. TRANSLATORS: This is the name of the Alt key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr ""

#. TRANSLATORS: This is the name of the Shift key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr ""

#. TRANSLATORS: This is the name of the Escape key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr ""

#. TRANSLATORS: This is the name of the Page Up key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr ""

#. TRANSLATORS: This is the name of the Page Down key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr ""

#. TRANSLATORS: This is the name of the Delete key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr ""

#. TRANSLATORS: This is the name of the Home key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr ""

#. TRANSLATORS: This is the name of the End key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr ""

#. TRANSLATORS: This is the name of the Tab key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:115
#, fuzzy
msgid "Tab"
msgstr "ਟੈਬਾਂ"

#. TRANSLATORS: This is the name of the Enter key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr ""

#: ../src/app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "ਠੀਕ"

#: ../src/app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr ""

#: ../src/app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:66
#, fuzzy
msgid "Font:"
msgstr "ਫ਼ੌਂਟ ਅਕਾਰ:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "ਫ਼ੌਂਟ ਅਕਾਰ:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr ""

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "ਖਾਕੇ"

#: ../src/app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr ""

#: ../src/app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr ""

#: ../src/app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr ""

#: ../src/app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:145
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:150
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:155
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:160
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:165
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:156
#, fuzzy
msgid "Close terminal"
msgstr "ਟਰਮੀਨਲ"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:171
#, fuzzy
msgid "Next tab"
msgstr "ਨਵੀਂ ਟੈਬ"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:175
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:185
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:190
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:195
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:200
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:205
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:210
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr ""

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr ""

#: ../src/app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr ""

#: ../src/app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr ""

#: ../src/app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr ""

#: ../src/app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "ਟੈਬਾਂ"

#: ../src/app/qml/TerminalPage.qml:63 ubuntu-terminal-app.desktop.in.in.h:1
msgid "Terminal"
msgstr "ਟਰਮੀਨਲ"

#: ../src/app/qml/TerminalPage.qml:302
msgid "Selection Mode"
msgstr "ਚੋਣ ਢੰਗ"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:291
#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:320
msgid "Un-named Color Scheme"
msgstr "ਬਿਨ-ਨਾਂ ਰੰਗ ਸਕੀਮ"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:468
msgid "Accessible Color Scheme"
msgstr "ਪਹੁੰਚਯੋਗ ਰੰਗ ਸਕੀਮ"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:533
msgid "Open Link"
msgstr "ਲਿੰਕ ਖੋਲ੍ਹੋ"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:534
msgid "Copy Link Address"
msgstr "ਲਿੰਕ ਪਤਾ ਕਾਪੀ ਕਰੋ"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:538
#, fuzzy
msgid "Send Email To…"
msgstr "...ਨੂੰ ਈਮੇਲ ਭੇਜੋ"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:539
msgid "Copy Email Address"
msgstr "ਈਮੇਲ ਪਤਾ ਕਾਪੀ ਕਰੋ"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:39
msgid "Match case"
msgstr "ਮਿਲਦੇ ਕੇਸ"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:45
msgid "Regular expression"
msgstr "ਨੇਮੀ ਸਮੀਕਰਨ"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:49
msgid "Higlight all matches"
msgstr "ਸਾਰੇ ਮੈਚਾਂ ਨੂੰ ਉਜਾਗਰ ਕਰੋ"

#: ../src/plugin/qmltermwidget/lib/Vt102Emulation.cpp:982
msgid ""
"No keyboard translator available.  The information needed to convert key "
"presses into characters to send to the terminal is missing."
msgstr ""
"ਕੋਈ ਕੀਬੋਰਡ ਅਨੁਵਾਦਕ ਉੱਪਲਬਧ ਨਹੀਂ। ਕੁੰਜੀਆਂ ਦਬਾਉਣ ਨੂੰ ਟਰਮੀਨਲ ਲਈ ਅੱਖਰਾਂ ਵਿੱਚ ਬਦਲਣ ਲਈ ਲੌੜੀਂਦੀ "
"ਜਾਣਕਾਰੀ ਗੁੰਮ ਹੈ।"

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:406
msgid "Color Scheme Error"
msgstr "ਰੰਗ ਸਕੀਮ ਖਾਮੀ"

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:407
#, qt-format
msgid "Cannot load color scheme: %1"
msgstr "ਰੰਗ ਸਕੀਮ ਲੋਡ ਨਹੀਂ ਕਰ ਸਕਦਾ: %1"

#~ msgid "Color Scheme"
#~ msgstr "ਰੰਗ ਸਕੀਮ"

#~ msgid "Ok"
#~ msgstr "ਠੀਕ"

#~ msgid "Settings"
#~ msgstr "ਸੈਟਿੰਗ'ਸ"

#~ msgid "Show Keyboard Bar"
#~ msgstr "ਕੀਬੋਰਡ ਪੱਟੀ ਵਿਖਾਓ"

#~ msgid "Enter password:"
#~ msgstr "ਪਾਸਵਰਡ ਦਾਖਲ ਕਰੋ:"

#~ msgid "password"
#~ msgstr "ਪਾਸਵਰਡ"
